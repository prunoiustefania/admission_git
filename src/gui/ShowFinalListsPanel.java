package gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import business.*;
import controllers.*;

public class ShowFinalListsPanel extends JPanel{
	
	private JLabel titleLabel;
	private JButton showButton;
	
	private Vector sectionsVector;
	private JList sectionsJList;
	private JList studentsJList;
	
	private JSplitPane splitPane;
	private JLabel potentialStudentsLabel;
	private JPanel potentialStudentsPane;
	
	private GridBagConstraints gbc;
	
	private ShowFinalListsController showFinalListsController;

	
	public ShowFinalListsPanel(ShowFinalListsController showFinalListsController) {
		super();
		this.showFinalListsController = showFinalListsController;
		initialize();
		gbc.insets = new Insets(10, 10, 10, 10);
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.BOTH;
		titleLabel.setFont(new Font("Burgues", Font.BOLD, 60));
		set_gbc(0, 1, 1, 1, GridBagConstraints.CENTER);
		add(titleLabel, gbc);
		
		
		gbc.gridwidth = 1;
		set_gbc(1, 1, 1, 1, GridBagConstraints.WEST);
		add(showButton, gbc);
	}


	public void initialize() {
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		titleLabel = new JLabel("Afisare liste finale", JLabel.CENTER);
		showButton = new JButton("Afisare studenti");
		splitPane = new JSplitPane();
		potentialStudentsPane = new JPanel();
		potentialStudentsLabel = new JLabel("Lista studentilor potential admisi : ", JLabel.CENTER);
		gbc = new GridBagConstraints();
		
		sectionsVector = new Vector();
		sectionsJList = new JList();
		studentsJList = new JList();
		
	}
	
	public void set_gbc(int row, int column, int width, int height, int anchor) {
		   gbc.gridy = row;
		   gbc.gridx = column;
		   gbc.gridwidth = width;
		   gbc.gridheight = height;
		   gbc.anchor = anchor;  
	}
	
	public void addToPanel() {
		
		repaint();
		revalidate();
		showButton.setText("");
		
		SectionsGroup sectionsGroup = showFinalListsController.getSectionsGroup();
		sectionsVector = sectionsGroup.getVectorOfOptions();
		sectionsJList = new JList<>(sectionsVector);
		sectionsJList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		splitPane.setLeftComponent(new JScrollPane(sectionsJList));
		
		splitPane.setRightComponent(new JLabel());
		
		sectionsJList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				ISection section = (ISection) sectionsJList.getSelectedValue();
				
				Vector finalVectorForEachSection = new Vector(section.getFinalVector());
				studentsJList = new JList(finalVectorForEachSection);
				studentsJList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
				//splitPane.setRightComponent(arg0);
				splitPane.setRightComponent(studentsJList);
				
				
			}
		});
		
		set_gbc(2, 1, 1, 1, GridBagConstraints.WEST);
		
		add(splitPane, gbc);
		
		potentialStudentsPane.add(potentialStudentsLabel);
		potentialStudentsPane.add(new JList<IStudent>(
		showFinalListsController.getStudentsGroup().getPotentialAdd()));
		set_gbc(3, 1, 1, 1, GridBagConstraints.WEST );
		gbc.weightx = 1;
		gbc.weighty = 1;
		add(potentialStudentsPane, gbc);
		
	}
	
	public void addShowButtonListener(ActionListener listenerForSumbitButton) {
		showButton.addActionListener(listenerForSumbitButton);
	}
	
	public void hideButton() {
		
		showButton.setVisible(false);
		repaint();
		revalidate();
	}
	
}
