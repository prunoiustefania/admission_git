package gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;

import controllers.*;
import business.*;

public class ProfilePanel extends JPanel{
	
	private JLabel lblTitle;
	private JLabel lblName;
	private JLabel lblNrBudget;
	private JLabel lblNrTax;
	private JLabel lblWayEval;
	private GridBagConstraints gbc;
	
	private JTextField nameOfProfileField;
	private JFormattedTextField numberOfBudgetField;
	private JTextField numberOfTaxField;
	private JComboBox waysOfEvalList;
	
	private JButton submitButton;
	private JButton nextProfileButton;
	
	private ProfilePanelController profilePanelController;
	
	
	public ProfilePanel(ProfilePanelController profilePanelController) {
		
		this.profilePanelController = profilePanelController;
		setLayout(new GridBagLayout());
		
		initialize();
		addToPanel();
	}
	
	public void initialize() {
	
		lblTitle = new JLabel("Configurare profil");
		lblName = new JLabel("Nume: ");
		lblNrBudget = new JLabel("Numar locuri la buget: ");
		lblNrTax = new JLabel("Numar locuri la taxa: ");
		lblWayEval = new JLabel("Alegeti modalitatea de evaluare: ");
	
		gbc = new GridBagConstraints();
		
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setMaximumFractionDigits(0);
		format.setMinimumFractionDigits(0);
		format.setParseIntegerOnly(true);
		
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setMaximum(1000);
		formatter.setMinimum(0);
		
		nameOfProfileField = new JTextField();
		numberOfBudgetField = new JFormattedTextField(formatter);
	    numberOfTaxField = new JFormattedTextField(formatter);

	    profilePanelController.createVector();
	    waysOfEvalList = new JComboBox<>(profilePanelController.getVector());

	    submitButton = new JButton("Submit");
		nextProfileButton = new JButton("Urmatorul profil");
		
	}
	
	public void set_gbc(int row, int column, int width, int height, int anchor) {
		   gbc.gridy = row;
		   gbc.gridx = column;
		   gbc.gridwidth = width;
		   gbc.gridheight = height;
		   gbc.anchor = anchor;  
	}
	
	public void addToPanel() {
		
		Dimension dim = new Dimension(500, 30);
		gbc.insets = new Insets(30, 30, 30, 30);
        gbc.gridwidth = 4;
        gbc.ipady = 20;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        lblTitle.setFont(new Font("Burgues", Font.BOLD, 60));
        set_gbc(0, 1, 1, 1, GridBagConstraints.CENTER);
		add(lblTitle, gbc);
		
		gbc.gridwidth = 1;
		set_gbc(1, 1, 1, 1, GridBagConstraints.WEST);
		lblName.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblName, gbc);
		
	
		nameOfProfileField.setPreferredSize(dim);
		nameOfProfileField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 35));
		set_gbc(1, 2, 1, 1, GridBagConstraints.CENTER);
		add(nameOfProfileField, gbc);
		
		set_gbc(2, 1, 1, 1, GridBagConstraints.WEST);
		lblNrBudget.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblNrBudget, gbc);
		
		set_gbc(2, 2, 1, 1, GridBagConstraints.CENTER);
		numberOfBudgetField.setPreferredSize(dim);
		numberOfBudgetField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 35));
		add(numberOfBudgetField, gbc);
		
		
		set_gbc(3, 1, 1, 1, GridBagConstraints.WEST);
		lblNrTax.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblNrTax, gbc);
		
		set_gbc(3, 2, 1, 1, GridBagConstraints.CENTER);
		numberOfTaxField.setPreferredSize(dim);
		numberOfTaxField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 35));
		add(numberOfTaxField, gbc);
	
		set_gbc(4, 1, 1, 1, GridBagConstraints.WEST);
		lblWayEval.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblWayEval, gbc);
		
		set_gbc(4, 2, 1, 1, GridBagConstraints.WEST);
		waysOfEvalList.setSize(dim);
		waysOfEvalList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 35));
		add(waysOfEvalList, gbc);
		
		submitButton.setSize(dim);
		nextProfileButton.setSize(dim);
		set_gbc(5, 1, 1, 1, GridBagConstraints.WEST);
		add(submitButton, gbc);
		set_gbc(5, 2, 1, 1, GridBagConstraints.WEST);
		add(nextProfileButton, gbc);
	}
	
	public String getName() {
		
		return nameOfProfileField.getText();
	}
	
	public int getNumberBudget() {
		
		return Integer.parseInt(numberOfBudgetField.getText());
	}
	
	public int getNumberTax() {
		
		return Integer.parseInt(numberOfTaxField.getText());
	}
	
	public IEvaluation getEvaluation() {
		
		return (IEvaluation) waysOfEvalList.getSelectedItem();
	}
	
	public void addSubmitListener(ActionListener listenerForSumbitButton) {
		submitButton.addActionListener(listenerForSumbitButton);
	}

	public void addNextProfileListener(ActionListener listenerForNextStudentButton) {
		nextProfileButton.addActionListener(listenerForNextStudentButton);
	}
	
	public void addNameOfProfileFieldListener(DocumentListener listenerForNameField) {
		nameOfProfileField.getDocument().addDocumentListener(listenerForNameField);
	}
	
	public void empyFields() {
		
		nameOfProfileField.setText(null);
		numberOfBudgetField.setText(null);
		numberOfTaxField.setText(null);
		waysOfEvalList.setSelectedIndex(0);
		
	}
	
	public boolean checkFields() {
		
		if(nameOfProfileField.getText().isEmpty()) {
			return true;
		}else if(numberOfBudgetField.getText().isEmpty()) {
			return true;
		}else if(numberOfTaxField.getText().isEmpty()) {
			return true;
		}
		
		return false;
	}

}
