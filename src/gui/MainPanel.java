package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import business.Repository;

public class MainPanel {
	
	private JFrame frame;
	private JPanel welcomeLabel;
	private StudentPanel studentPanel;
	private ProfilePanel profilePanel;
	private ExamGradesPanel examGradesPanel;
	
	private ShowListsPanel showFirstListsPanel;
	private ConfirmationPanel confirmationPanel;
	private ShowFinalListsPanel showFinalListsPanel;
	private Repository repository;
	
	public MainPanel(StudentPanel studentPanel, ProfilePanel profilePanel, ExamGradesPanel examGradesPanel,
					 ShowListsPanel showFirstListsPanel, ConfirmationPanel confirmationPanel, 
					 ShowFinalListsPanel showFinalListsPanel, Repository repository) {
		
		this.studentPanel = studentPanel;
		this.profilePanel = profilePanel;
		this.examGradesPanel = examGradesPanel;
		this.showFirstListsPanel = showFirstListsPanel;
		this.confirmationPanel = confirmationPanel;
		this.showFinalListsPanel = showFinalListsPanel;
		this.repository = repository;
		frame = new JFrame(" TEST ");
		Dimension dimMax = new Dimension();
		dimMax = Toolkit.getDefaultToolkit().getScreenSize();
		System.out.println(dimMax);
		
		frame.setMinimumSize(dimMax);
		//frame.setResizable(false);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLocationRelativeTo(null);
	    frame.getContentPane().setLayout(new BorderLayout());
	    initialize();
	    frame.pack();
	    frame.setVisible(true);
	    frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	serialize();
            	e.getWindow().dispose();
            }
        });
	}
	
	private void serialize() {
		try {

            FileOutputStream fileOut = new FileOutputStream("./repository.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(repository);
            out.close();
            fileOut.close();

        } catch (IOException i) {

            i.printStackTrace();

        }		
	}

	private void initialize() {
		
		 welcomeLabel = new JPanel();
		 ImageIcon icon = new ImageIcon("welcome.png"); 
		 welcomeLabel.add(new JLabel(icon));
		
		 JPanel contentPanel = new JPanel();
		 Controller controller = new Controller(contentPanel);
		 
		 MenuPanel admissionMenu = new MenuPanel("Admitere"); 
		 
		 controller.cleanPanel();
		 controller.switchPanel(welcomeLabel);
		 
		 
		 admissionMenu.addAction(new AbstractMenuAction("Configurare profil") {
			
			public void actionPerformed(ActionEvent arg0) {
				
				controller.cleanPanel();
				controller.switchPanel(profilePanel);
				
			}
		 });
		 
		 admissionMenu.addAction(new AbstractMenuAction("Add Students") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				controller.cleanPanel();
				controller.switchPanel(studentPanel);
				
				
			}
		 });
		 
		 admissionMenu.addAction(new AbstractMenuAction("Adaugare note examen") {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					controller.cleanPanel();
					controller.switchPanel(examGradesPanel);
					
				}
			});
		 
		 admissionMenu.addAction(new AbstractMenuAction("Liste preliminare") {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					controller.cleanPanel();
					controller.switchPanel(showFirstListsPanel);
					
				}
			});
		 
		 admissionMenu.addAction(new AbstractMenuAction("Confirmari") {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					controller.cleanPanel();
					controller.switchPanel(confirmationPanel);
					
				}
			});
		 
		 admissionMenu.addAction(new AbstractMenuAction("Liste finale") {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					controller.cleanPanel();
					controller.switchPanel(showFinalListsPanel);
					
				}
			});
		 
		 frame.add(contentPanel);
		 frame.add(admissionMenu, BorderLayout.WEST);
		 
	}
}
