package gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import business.*;
import controllers.StudentPanelController;

public class StudentPanel extends JPanel implements Observer{
	

	private JLabel lblTitle;
	private JLabel lblName;
	private JLabel lblCnp;
	private JLabel lblAvarageGrade;
	private JLabel lblFirstGrade;
	private JLabel lblSecondGrade;
	private JLabel lblThirdGrade;
	private JLabel lblChooseOptions;
	private GridBagConstraints gbc;
	
	private JTextField nameOfStudentField;
	private JTextField cnpField;
	private JTextField avarageGradeField;
	private JTextField firstGradeField;
	private JTextField secondGradeField;
	private JTextField thirdGradeField;
	private JComboBox chooseOptionsList;// from SectionsGroup;
	private JTextField chosenOptionsField;
	
	private JButton addSectionButton;
	private JButton submitButton;
	private JButton nextStudentButton;
	
	private StudentPanelController studentPanelController;
	
	
	public StudentPanel(StudentPanelController studentPanelController) {
		
		this.studentPanelController = studentPanelController;
		setLayout(new GridBagLayout());
		
		initialize();
		addToPanel();
		
	}
	
	public void initialize() {
		
		lblTitle = new JLabel("Adaugare student");
		lblName = new JLabel("Nume : ");
		lblCnp = new JLabel("CNP : ");
		lblAvarageGrade = new JLabel("Medie bacalaureat : ");
		lblFirstGrade = new JLabel("Nota Limba Romana : ");
		lblSecondGrade = new JLabel("Nota Matematica : ");
		lblThirdGrade = new JLabel("Nota materi la alegere : ");
		lblChooseOptions = new JLabel("Sectii disponibile : ");
		gbc = new GridBagConstraints();
		
		DecimalFormat format = new DecimalFormat("#.##");
		
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setMaximum(10.0);
		formatter.setMinimum(1.0);
	
		nameOfStudentField = new JTextField();
		cnpField = new JTextField();
	    avarageGradeField = new JFormattedTextField(formatter);
	    firstGradeField = new JFormattedTextField(formatter);
	    secondGradeField = new JFormattedTextField(formatter);
	    thirdGradeField = new JFormattedTextField(formatter);
	    chooseOptionsList = new JComboBox<>();
	    chosenOptionsField = new JTextField();
	    
	    
	    addSectionButton = new JButton("Adauga sectie: ");
	    submitButton = new JButton("Submit");
		nextStudentButton = new JButton("Studentul urmator");
		
	}
	
	public void set_gbc(int row, int column, int width, int height, int anchor) {
		   gbc.gridy = row;
		   gbc.gridx = column;
		   gbc.gridwidth = width;
		   gbc.gridheight = height;
		   gbc.anchor = anchor;  
	}
	
	public void addToPanel() {
		
		Dimension dim = new Dimension(400, 40);
		gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        lblTitle.setFont(new Font("Burgues", Font.BOLD, 60));
        set_gbc(0, 1, 1, 1, GridBagConstraints.CENTER);
		add(lblTitle, gbc);
		
		gbc.gridwidth = 1;
		set_gbc(1, 1, 1, 1, GridBagConstraints.WEST);
		lblName.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblName, gbc);
		
	
		nameOfStudentField.setPreferredSize(dim);
		set_gbc(1, 2, 1, 1, GridBagConstraints.CENTER);
		nameOfStudentField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(nameOfStudentField, gbc);
		
		set_gbc(2, 1, 1, 1, GridBagConstraints.WEST);
		lblCnp.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblCnp, gbc);
		
		set_gbc(2, 2, 1, 1, GridBagConstraints.CENTER);
		cnpField.setPreferredSize(dim);
		cnpField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(cnpField, gbc);
		
		set_gbc(3, 1, 1, 1, GridBagConstraints.WEST);
		lblAvarageGrade.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblAvarageGrade, gbc);
		
		set_gbc(3, 2, 1, 1, GridBagConstraints.CENTER);
		avarageGradeField.setPreferredSize(dim);
		avarageGradeField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(avarageGradeField, gbc);
	
		set_gbc(4, 1, 1, 1, GridBagConstraints.WEST);
		lblFirstGrade.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblFirstGrade, gbc);
		
		set_gbc(4, 2, 1, 1, GridBagConstraints.CENTER);
		firstGradeField.setPreferredSize(dim);
		firstGradeField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(firstGradeField, gbc);
		
		set_gbc(5, 1, 1, 1, GridBagConstraints.WEST);
		lblSecondGrade.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblSecondGrade, gbc);
		
		set_gbc(5, 2, 1, 1, GridBagConstraints.CENTER);
		secondGradeField.setPreferredSize(dim);
		secondGradeField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(secondGradeField, gbc);
		
		set_gbc(6, 1, 1, 1, GridBagConstraints.WEST);
		lblThirdGrade.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblThirdGrade, gbc);
		
		set_gbc(6, 2, 1, 1, GridBagConstraints.CENTER);
		thirdGradeField.setPreferredSize(dim);
		thirdGradeField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(thirdGradeField, gbc);
		
		set_gbc(7, 1, 1, 1, GridBagConstraints.WEST);
		lblChooseOptions.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		add(lblChooseOptions, gbc);
		
		
		set_gbc(8, 1, 1, 1, GridBagConstraints.WEST);
		addSectionButton.setPreferredSize(dim);
		add(addSectionButton, gbc);
		
		set_gbc(8, 2, 1, 1, GridBagConstraints.WEST);
		chosenOptionsField.setPreferredSize(dim);
		add(chosenOptionsField, gbc);
		
		set_gbc(9, 1, 1, 1, GridBagConstraints.WEST);
		submitButton.setPreferredSize(new Dimension(400,50));
		add(submitButton, gbc);
		set_gbc(9, 2, 1, 1, GridBagConstraints.WEST);
		nextStudentButton.setPreferredSize(new Dimension(400,50));
		add(nextStudentButton, gbc);
	}
	
	
	public String getName() {
		
		return nameOfStudentField.getText();
		
	}
	
	public String getCnp() {
		
		return cnpField.getText();
	}
	
	public Double getAvarageGrade() {
		
		String grade = avarageGradeField.getText();
		return Double.parseDouble(grade);
	}
	
    public Double getFirstGrade() {
		
		return Double.parseDouble(firstGradeField.getText());
	}
    
    public Double getSecondGrade() {
		
		return Double.parseDouble(secondGradeField.getText());
	}
    
    public Double getThirdGrade() {
		
		return Double.parseDouble(thirdGradeField.getText());
	}
    
    public ISection getOptionSelected() {
    	
    	return (ISection) chooseOptionsList.getSelectedItem();
    }
    
    public void addAddSectionListener(ActionListener listenerForAddSectionListener) {
    	addSectionButton.addActionListener(listenerForAddSectionListener);
    }
    
	public void addSubmitListener(ActionListener listenerForSumbitButton) {
		submitButton.addActionListener(listenerForSumbitButton);
	}

	public void addNextStudentListener(ActionListener listenerForNextStudentButton) {
		nextStudentButton.addActionListener(listenerForNextStudentButton);
	}

    public void emptyFields() {
    	
    	nameOfStudentField.setText(null);
		cnpField.setText(null);
		avarageGradeField.setText(null);
		firstGradeField.setText(null);
		secondGradeField.setText(null);
		thirdGradeField.setText(null);
		chooseOptionsList.setSelectedIndex(0);
		chosenOptionsField.setText(" ");
    }


	@Override
	public void update(Observable arg0, Object arg1) {
		
		
		SectionsGroup sectionsGroup = (SectionsGroup) arg1;
		
		remove(chooseOptionsList);
		
		chooseOptionsList = new JComboBox<>(sectionsGroup.getVectorOfOptions());
		
		set_gbc(7, 2, 1, 1, GridBagConstraints.CENTER);
		chooseOptionsList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		add(chooseOptionsList, gbc);
		
		
		revalidate();
		repaint();
		
	}
	
	public void setText(String string) {
		
		chosenOptionsField.setText(string);
		chosenOptionsField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 30));
		chosenOptionsField.setAutoscrolls(true);
	}

	public boolean checkFields() {
		if(nameOfStudentField.getText().isEmpty()) {
			return true;
		}else if(cnpField.getText().isEmpty()) {
			return true;
		}else if(avarageGradeField.getText().isEmpty()) {
			return true;
		}else if(firstGradeField.getText().isEmpty()) {
			return true;
		}else if(secondGradeField.getText().isEmpty()) {
			return true;
		}else if(thirdGradeField.getText().isEmpty()) {
			return true;
		}
	
		return false;
	}

}
