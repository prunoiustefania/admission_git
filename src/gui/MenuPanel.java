package gui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MenuPanel extends JPanel {

    public MenuPanel(String name) {
        setLayout(new GridLayout(0, 1));
        setBorder(new LineBorder(Color.DARK_GRAY));
        add(new JLabel(name, JLabel.CENTER));
 
    }

    public void addAction(AbstractMenuAction action) {
        JButton btn = new JButton(action);
        //btn.add(new JLabel(new ImageIcon("graduate.png")));
        add(btn);
    }
	
}
