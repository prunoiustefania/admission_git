package gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import business.IStudent;
import business.StudentsGroup;
import controllers.*;

public class ConfirmationPanel extends JPanel implements Observer {
	
	private ConfirmationController confirmationController;
	
    private JLabel titleLabel;
    private StudentsGroup studentsGroup;
    private JList<IStudent> studentsJList;
    private JSplitPane splitPane;
	private JPanel informationPanel;
    private JRadioButton aRadioButton;
    private JRadioButton bRadioButton;
    private ButtonGroup firstRadioButtonGroup;
    private JButton showStudents;
    private JButton updateConfButton;
    private GridBagConstraints gbc;
	
	public ConfirmationPanel(ConfirmationController confirmationController) {
		
		this.confirmationController = confirmationController;
		initialize();
		
		gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		titleLabel.setFont(new Font("Burgues", Font.BOLD, 60));
		set_gbc(0, 1, 1, 1, GridBagConstraints.CENTER);
		add(titleLabel, gbc);
		
		
		gbc.gridwidth = 1;
		set_gbc(1, 1, 1, 1, GridBagConstraints.WEST);
		add(showStudents, gbc);
		
	}
	
	public void initialize() {
		
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		titleLabel = new JLabel("Confirmari", JLabel.CENTER);
		studentsJList = new JList();
		splitPane = new JSplitPane();
		informationPanel = new JPanel();
		showStudents = new JButton("Afisare studenti");
		updateConfButton = new JButton("Inregistrare confirmare");
		
        aRadioButton = new JRadioButton("A confirmat");
        bRadioButton = new JRadioButton("Nu a confirmat");
        firstRadioButtonGroup = new ButtonGroup();
	}
	
	public void set_gbc(int row, int column, int width, int height, int anchor) {
		   gbc.gridy = row;
		   gbc.gridx = column;
		   gbc.gridwidth = width;
		   gbc.gridheight = height;
		   gbc.anchor = anchor;  
	}
	
	public void addToPanel() {
		
		repaint();
		revalidate();
		showStudents.setText("");
		studentsJList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		studentsJList.setSelectedIndex(0);
		splitPane.setLeftComponent(new JScrollPane(studentsJList));
		
		aRadioButton.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		bRadioButton.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		updateConfButton.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		firstRadioButtonGroup.add(aRadioButton);
	    firstRadioButtonGroup.add(bRadioButton);
	    firstRadioButtonGroup.clearSelection();
	    aRadioButton.setSelected(true);
			
		informationPanel.add(aRadioButton);
		informationPanel.add(bRadioButton);
		informationPanel.add(updateConfButton);
		splitPane.setRightComponent(informationPanel);
		//splitPane.setDividerLocation(0.5);
		set_gbc(2, 1, 1, 1, GridBagConstraints.WEST);
		gbc.weightx = 10;
		gbc.weighty = 10;
		add(splitPane, gbc);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		studentsGroup = (StudentsGroup) arg1;
		studentsJList = new JList(studentsGroup.getAllStudents());
		
		studentsJList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				IStudent student = studentsJList.getSelectedValue();
				
			}
		});
		
	}
	
	public void addShowStudentsListener(ActionListener listenerForShowStudents) {
		showStudents.addActionListener(listenerForShowStudents);
	}
	
	public void addaRadioButtonListener(ActionListener listenerForaRadioButton) {
		aRadioButton.addActionListener(listenerForaRadioButton);
	}
	
	public void addbRadioButtonListener(ActionListener listenerForbRadioButton) {
		bRadioButton.addActionListener(listenerForbRadioButton);
	}
	
	public void addUpdateConfButtonListener(ActionListener listenerForUpdateConfButton) {
		updateConfButton.addActionListener(listenerForUpdateConfButton);
	}
	
	public IStudent getStudent() {
		
		IStudent student = studentsJList.getSelectedValue();
		return student;
	}
	

}
