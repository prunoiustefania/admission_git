package gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import java.util.function.DoublePredicate;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import business.*;
import controllers.ExamGradesController;
import gui.*;

public class ExamGradesPanel extends JPanel implements Observer{
	
	private JLabel titleLabel;
	private Vector<IStudent> studentsVector = new Vector();
	private JSplitPane splitPane;
	private JList<IStudent> studentsJList;
	private JPanel informationPanel;
	private JLabel studentLabel;
	private JTextField gradeField;
	
	private IStudent auxStudent;
	
	private GridBagConstraints gbc;
	
	private JButton submitButton;
	private JButton showStudents;
	
	private ExamGradesController examGradesController;
	

	public ExamGradesPanel(ExamGradesController examGradesController) {
		
		this.examGradesController = examGradesController;
		initialize();
		
		gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        titleLabel.setFont(new Font("Burgues", Font.BOLD, 30));
        set_gbc(0, 1, 1, 1, GridBagConstraints.CENTER);
		add(titleLabel, gbc);
		
		gbc.gridwidth = 1;
		set_gbc(1, 1, 1, 1, GridBagConstraints.WEST);
		add(showStudents, gbc);
		
	}
	

	public void initialize() {
		
		setLayout(new GridBagLayout());
		
		titleLabel = new JLabel("Adaugare note exam", JLabel.CENTER);
		gbc = new GridBagConstraints();
		submitButton = new JButton(" Submit ");
		submitButton.setPreferredSize(new Dimension(200, 30));
		showStudents = new JButton("Afisare studenti");
		showStudents.setSize(new Dimension(200, 30));
		
		splitPane = new JSplitPane();
		informationPanel = new JPanel();
		studentLabel = new JLabel(); 
		gradeField = new JTextField();
		gradeField.setPreferredSize(new Dimension(200, 40));
		
		
	}
	
	public void set_gbc(int row, int column, int width, int height, int anchor) {
		   gbc.gridy = row;
		   gbc.gridx = column;
		   gbc.gridwidth = width;
		   gbc.gridheight = height;
		   gbc.anchor = anchor;  
	}
	
	
	public void addToPanel() {
		
		repaint();
		revalidate();
	
		
		studentsJList.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		splitPane.setLeftComponent(new JScrollPane(studentsJList));
		gradeField.setFont(new Font("Burgues", Font.ROMAN_BASELINE, 40));
		informationPanel.add(gradeField);	
		submitButton.setPreferredSize(new Dimension(400, 40));
		informationPanel.add(submitButton);
		
		studentsJList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				auxStudent = studentsJList.getSelectedValue();
				
				
			}
		});
	
		
		splitPane.setRightComponent(informationPanel);
	
		set_gbc(2, 1, 1, 1, GridBagConstraints.WEST);
		gbc.weightx = 1;
		gbc.weighty = 1;
		add(splitPane, gbc);
	
	}
	
	public void addToLabel() {
		
		studentLabel.add(gradeField);
		studentLabel.add(submitButton);
		informationPanel.add(studentLabel);
		splitPane.setRightComponent(informationPanel);
		repaint();
		revalidate();
	}
	

	@Override
	public void update(Observable arg0, Object arg1) {
		
		StudentsGroup studentsGroup = (StudentsGroup) arg1;
		studentsVector = studentsGroup.getStudentsWhoNeedExamGroup();
		studentsJList = new JList<>(studentsVector);
		
		
	}
	
	public void addShowStudentsListener(ActionListener listenerForShowStudents) {
		showStudents.addActionListener(listenerForShowStudents);
	}
	

	public void addSubmitListener(ActionListener listenerForSumbitButton) {
		submitButton.addActionListener(listenerForSumbitButton);
	}
	
	public Double getGrade() {
		
		return Double.parseDouble(gradeField.getText());
	}
	
	public void addGradeToStudent() {
		
		auxStudent.addExamGrade(getGrade());
		System.out.println(auxStudent.getExamGrade());
	}

	
	public void clearTextField() {
		
		gradeField.setText("");
	}
	
	public void hideButton() {
		
		showStudents.setVisible(false);
	}
}
