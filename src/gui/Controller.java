package gui;

import java.awt.GridLayout;

import javax.swing.JPanel;

public class Controller {
	
	private JPanel contentPanel;
	
	public Controller(JPanel contentPanel) {
		this.contentPanel = contentPanel;
		contentPanel.setLayout(new GridLayout(1, 1));
	}
	
	public void switchPanel(JPanel othercontentPanel) {
		
		contentPanel.removeAll();
		contentPanel.add(othercontentPanel);
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void cleanPanel() {
		
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.repaint();
	}

}
