package gui;


import javax.swing.AbstractAction;

public abstract class AbstractMenuAction extends AbstractAction implements IMenuAction {
	
	
	AbstractMenuAction(String name){
		putValue(NAME,name);
	}

}
