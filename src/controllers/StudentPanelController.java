package controllers;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import gui.*;
import business.*;
import controllers.ProfilePanelController.NextProfileListener;
import controllers.ProfilePanelController.SubmitListener;

public class StudentPanelController extends Observable{
	
	private StudentPanel studentPanel;
	private SectionsGroup sectionsGroup;
	private List<ISection> chosenOptionsList;
	
	private StudentsGroup studentsGroup;
	private StudentBuilder studentBuilder;

	
	private IStudent student;
	private String nameStudent, cnpStudent;

	
	public StudentPanelController(SectionsGroup sectionsGroup,
			List<ISection> chosenOptionsList, StudentsGroup studentsGroup,
			StudentBuilder studentBuilder) {

		this.sectionsGroup = sectionsGroup;
		this.chosenOptionsList = chosenOptionsList;
		this.studentsGroup = studentsGroup;
		this.studentBuilder = studentBuilder;
	}

	public void setPanel( StudentPanel studentPanel) {
		
		this.studentPanel = studentPanel;
		this.studentPanel.addAddSectionListener(new AddSectionListener());
		this.studentPanel.addSubmitListener(new SubmitListener());
		this.studentPanel.addNextStudentListener(new NextStudentListener());
		
	}
	
	public Vector<ISection> getOptionsList() {
		return sectionsGroup.getVectorOfOptions();
	}
	
	class AddSectionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			chosenOptionsList.add(studentPanel.getOptionSelected());
			
			studentPanel.setText(chosenOptionsList.toString());
			
			
		}
	}
	
	class SubmitListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			if(studentPanel.checkFields()){
			    JOptionPane.showMessageDialog(null,
			    		"Completati toat campurile", "TextField invalid",
			    		JOptionPane.ERROR_MESSAGE);
			    return;
			}
			nameStudent = studentPanel.getName();
			cnpStudent = studentPanel.getCnp();
			
			if(cnpStudent.length() > 13) {
				JOptionPane.showMessageDialog(null, "Introduceti un CNP valid", "TextField invalid", JOptionPane.ERROR_MESSAGE);
			    return;
			}
			
			IGrades grades = new Grades(new ArrayList());
			
			grades.add(studentPanel.getAvarageGrade());
			grades.add(studentPanel.getFirstGrade());
			grades.add(studentPanel.getSecondGrade());
			grades.add(studentPanel.getThirdGrade());
			
			ArrayList<ISection> array = new ArrayList<>();
			
			array.addAll(chosenOptionsList);
			if(array.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Completati toat campurile", "TextField invalid", JOptionPane.ERROR_MESSAGE);
			    return;
			}
			student = studentBuilder.create(studentPanel.getName(),
					studentPanel.getCnp(), grades, array);
			
			student.putInSectionsList();
			
			studentsGroup.addToGroup(student);
			studentsGroup.addToExamGroup(student);

		}	
	}
	
	class NextStudentListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
		
			studentPanel.emptyFields();
			
			setChanged();
			notifyObservers(studentsGroup);
			
			chosenOptionsList.clear();
			
		}
	}

}
