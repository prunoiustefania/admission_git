package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import business.*;
import gui.*;

public class ProfilePanelController  extends Observable{
	
	private Vector<IEvaluation> wayOfEvaluation; 
	private EvaluationsBuilder evalBuilder;
	private SectionBuilder sectionBuilder;
	private PlacesBuilder placesBuilder;
	private SectionsGroup sectionsGroup;
	
	private ProfilePanel profilePanel;
	private MainPanel mainPanel;
	
	
	private String nameProfile;
	private int numberBudget, numberTax;
	private IEvaluation chosenEvaluation;
	
	
	public ProfilePanelController(EvaluationsBuilder evalBuilder, SectionBuilder sectionBuilder,
								  PlacesBuilder placesBuilder, SectionsGroup sectionsGroup) {
		
		this.evalBuilder = evalBuilder;
		this.sectionBuilder = sectionBuilder;
		this.placesBuilder = placesBuilder;
		this.sectionsGroup = sectionsGroup;
		setChanged();
		notifyObservers(this.sectionsGroup);
	}
	
	public void setPanel( ProfilePanel profilePanel) {
		
		this.profilePanel = profilePanel;
		this.profilePanel.addSubmitListener(new SubmitListener());
		this.profilePanel.addNextProfileListener(new NextProfileListener());
		
	}
	
	
	public void createVector() {
		
		wayOfEvaluation = evalBuilder.createVector();
	}
	
	public Vector<IEvaluation> getVector(){
		
		return wayOfEvaluation;
	}
	
	class SubmitListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			if(profilePanel.checkFields()){
			    JOptionPane.showMessageDialog(null,
			    		"Completati toat campurile", "Invalid TextFields",
			    		JOptionPane.ERROR_MESSAGE);
			    return;
			}
			
			nameProfile = profilePanel.getName();
			
			if(sameName(nameProfile)) {
				 JOptionPane.showMessageDialog(null, "A fost introdus acest profil", 
						 "Invalid TextFields", JOptionPane.ERROR_MESSAGE);
				 return;
			}
			
			numberBudget = profilePanel.getNumberBudget();
			
			numberTax = profilePanel.getNumberTax();
			chosenEvaluation = profilePanel.getEvaluation();
			
			if (numberBudget > 0) {
				sectionsGroup.addToGroup(sectionBuilder.create(nameProfile, 
						placesBuilder.createBudgetPlaces(numberBudget),
			              chosenEvaluation));
				
			}
				
			
			if(numberTax > 0) {
				sectionsGroup.addToGroup(sectionBuilder.create(nameProfile, placesBuilder.createTaxPlaces(numberTax),
						              chosenEvaluation));
			}
		}

		private boolean sameName(String nameProfile) {
			for(int index = 0; index < 
					sectionsGroup.getVectorOfOptions().size(); index++) {
				ISection section = sectionsGroup.getVectorOfOptions().get(index);
				if(section.hasSameName(nameProfile)) {
					return true;
				}
			}
			
			return false;
		}	
	}
	
	class NextProfileListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
		
			profilePanel.empyFields();
			
			setChanged();
			notifyObservers(sectionsGroup);
		}
	}
}
