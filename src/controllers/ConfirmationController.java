package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import business.*;
import controllers.ExamGradesController.ShowStudents;
import gui.ConfirmationPanel;

public class ConfirmationController {
	
	private ConfirmationPanel confirmationPanel;
	private StudentsGroup studentsGroup;
	private boolean confirmationState = true;
	
	public void setPanel(ConfirmationPanel confirmationPanel) {
		
		this.confirmationPanel = confirmationPanel;
		this.confirmationPanel.addShowStudentsListener(new ShowStudents());
		this.confirmationPanel.addaRadioButtonListener(new ARadioButton());
		this.confirmationPanel.addbRadioButtonListener(new BRadioButton());
		this.confirmationPanel.addUpdateConfButtonListener(new UpdateConfButton());
	}

	
	class ShowStudents implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			confirmationPanel.addToPanel();
			
		}	
	}
	
	class ARadioButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			confirmationState = true;
			
		}
	}
	
	class BRadioButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			confirmationState = false;
		}
	}
	
	class UpdateConfButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			IStudent student = confirmationPanel.getStudent();
			student.confirmation(confirmationState);
			
		}
	}

}
