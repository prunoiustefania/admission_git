package controllers;
import business.*;
import gui.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JScrollPane;

public class ShowListsController implements Observer{

	private ShowListsPanel showListsPanel;
	
	private StudentsGroup studentsGroup;
	private SectionsGroup sectionsGroup;
	
	private Manage manage;
	
	public void setPanel(ShowListsPanel showListsPanel) {
		this.showListsPanel = showListsPanel;
		this.showListsPanel.addShowButtonListener(new ShowButtonListener());
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		
		if(arg1 instanceof SectionsGroup) {
			
			this.sectionsGroup = (SectionsGroup) arg1;
		}
		
		if(arg1 instanceof StudentsGroup) {
		
			this.studentsGroup = (StudentsGroup) arg1;
		}
		
	}
	
	public void initialize() {
		
		manage.setAdmGradeForStudents();
		manage.sortAndPrintStudents();
		manage.putStudentInHisFirstOption();
	}
	
	public SectionsGroup getSectionsGroup(){
		return sectionsGroup;
	}
	
	public StudentsGroup getStudentsGroup(){
		return studentsGroup;
	}
	class ShowButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (studentsGroup != null && sectionsGroup != null) {
				manage = new Manage(studentsGroup, sectionsGroup);
	
				initialize();
				
				showListsPanel.addToPanel();
				//showListsPanel.hideButton();
			}
			
		}	
	}
	
}
