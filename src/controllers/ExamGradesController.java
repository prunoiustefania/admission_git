package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import business.*;
import controllers.StudentPanelController.SubmitListener;
import gui.*;

public class ExamGradesController implements Observer{
	
	private StudentsGroup studentsGroup;
	private List<IStudent> studentsList = new ArrayList();
	
	private ExamGradesPanel examGradesPanel;
	private List<IStudent> studentsWhoNeedExamGrades;

	
	public void setPanel(ExamGradesPanel examGradesPanel) {
		this.examGradesPanel = examGradesPanel;
		this.examGradesPanel.addSubmitListener(new SubmitListener());
		this.examGradesPanel.addShowStudentsListener(new ShowStudents());
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		
		StudentsGroup studentsGroup = (StudentsGroup) arg1;
		studentsList = studentsGroup.getStudentsWhoNeedExamGroup();
		
	}
	
	class SubmitListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			examGradesPanel.addGradeToStudent();
			examGradesPanel.clearTextField();
			
		}	
	}
	
	class ShowStudents implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			examGradesPanel.addToPanel();
			//examGradesPanel.hideButton();
			
		}	
	}
	
}
