package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import controllers.ShowListsController.ShowButtonListener;
import gui.*;
import business.*;

public class ShowFinalListsController implements Observer {
	
	private ShowFinalListsPanel showFinalListsPanel;
	private StudentsGroup studentsGroup;
	private SectionsGroup sectionsGroup;
	private Manage manage;
	
	public void setPanel(ShowFinalListsPanel showFinalListsPanel) {
		this.showFinalListsPanel = showFinalListsPanel;
		this.showFinalListsPanel.addShowButtonListener(new ShowButtonListener());
	}
	
	public SectionsGroup getSectionsGroup(){
		return sectionsGroup;
	}
	
	public StudentsGroup getStudentsGroup() {
		return studentsGroup;
	}
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof SectionsGroup) {
			
			this.sectionsGroup = (SectionsGroup) arg1;
		}
		
		if(arg1 instanceof StudentsGroup) {
		
			this.studentsGroup = (StudentsGroup) arg1;
		}
		
	}
	
	class ShowButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			

			if (studentsGroup != null && sectionsGroup != null) {
				manage = new Manage(studentsGroup, sectionsGroup);
			}
			
			studentsGroup.confirmation();
			
			Vector <ISection> sectionsVector = new Vector(sectionsGroup.getVectorOfOptions());
			for (ISection section : sectionsVector) {
				section.refreshFinalList();
			}
			manage.sortAndPrintStudents();
			manage.putStudentInHisFirstOption();
			showFinalListsPanel.addToPanel();
			//showFinalListsPanel.hideButton();
			
		}
		
	}

}
