package business;

import java.io.Serializable;

public class ExamAvarageEvaluation implements IEvaluation, Serializable{

	@Override
	public boolean needExam() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public double evaluationGrade(IGrades grades) {
		// TODO Auto-generated method stub
		double evaluationGrade = (7 * grades.getExamGrade() + 3 * grades.getExamGrade()) / 10;
		
		return evaluationGrade;
	}

	@Override
	public String toString() {
		return "Dupa media de la Bacalaureat si nota de la exam";
	}

	
}
