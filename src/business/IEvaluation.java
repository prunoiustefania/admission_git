package business;

public interface IEvaluation {

	public boolean needExam();
	
	public double evaluationGrade(IGrades grades);
	
	public String toString();
	
}
