package business;
import java.io.Serializable;
import java.util.List;
import java.util.Observable;
import java.util.Vector;

public class SectionsGroup extends Observable implements Serializable{
	
	private List<ISection> sectionsGroup;
	private Vector<ISection> sectionsVector;

	
	public SectionsGroup(List<ISection> sectionsGroup) {
		super();
		this.sectionsGroup = sectionsGroup;
	}


	public void addToGroup(ISection section) {
		
		sectionsGroup.add(section);
	}
	
	public void setAdmGradeForSections() {
		
		for (ISection section : sectionsGroup) {
			
			section.setAddmGradeForStudents();
		}
	}


	public Vector<ISection> getVectorOfOptions() {
		// TODO Auto-generated method stub
		sectionsVector = new Vector<>(sectionsGroup);
	
		return sectionsVector;
	}
	
	
/*	public void printList() {
		
		for (ISection section : sectionsGroup) {
			System.out.println(" ----------- ");
			
			section.printList();
		}
	}
	
	
	// untile here


	public void print() {
		
		for(int index = 0; index < sectionsGroup.size(); index++) {
			System.out.println("SectionsGroup ["+ index + "] = " + sectionsGroup.get(index));
		}
	}
	*/
	
}
