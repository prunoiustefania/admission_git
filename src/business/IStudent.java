package business;

public interface IStudent{
	
	public boolean needExamGrade();
	
	public void putInSectionsList();
	
	public void evaluationGrade(IEvaluation evaluation);

	public int hasBetterGradeThan( IStudent student);
	
	public int isBetter(double grade);

	public boolean putStudentInHisFirstOption();
	
	public int hasBetterMathGradeThan( IStudent student);

	public int isBetterMath(double mathGrade);

	public void confirmation(boolean b);
	
	public boolean isConfirmed(boolean a);
	
	public String toString();

	public void addExamGrade(Double grade);
	
	public Double getExamGrade();
}
