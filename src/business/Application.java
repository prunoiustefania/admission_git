package business;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import controllers.ConfirmationController;
import controllers.ExamGradesController;
import controllers.ProfilePanelController;
import controllers.ShowFinalListsController;
import controllers.ShowListsController;
import controllers.StudentPanelController;
import gui.ConfirmationPanel;
import gui.Controller;
import gui.ExamGradesPanel;
import gui.MainPanel;
import gui.MenuPanel;
import gui.ProfilePanel;
import gui.ShowFinalListsPanel;
import gui.ShowListsPanel;
import gui.StudentPanel;


public class Application {
	
	private Repository repository;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application application = new Application();
		application.launchGui();
	}

	
	
	private void launchGui() {
		
		init();
		
		SectionsGroup sectionsGroup = repository.getSectionsGroup();
		 
		ProfilePanelController profilePanelController = 
				 new ProfilePanelController(new EvaluationsBuilder(), new SectionBuilder(),
						 					new PlacesBuilder(), sectionsGroup);
		

		List<ISection> chosenOptionsList = new ArrayList();

		StudentsGroup studentsGroup = repository.getStudentsGroup();
		StudentPanelController studentPanelController = new StudentPanelController(sectionsGroup,
				 chosenOptionsList, studentsGroup, new StudentBuilder());
		

		
		 ProfilePanel profilePanel = new ProfilePanel(profilePanelController);
		 profilePanelController.setPanel(profilePanel);
		 
		 StudentPanel studentPanel = new StudentPanel(studentPanelController);
		 studentPanelController.setPanel(studentPanel);
		 
		 
		 profilePanelController.addObserver(studentPanel);
		 
		 
		 ExamGradesController examGradesController =  new ExamGradesController(); 
		 ExamGradesPanel examGradesPanel = new ExamGradesPanel(examGradesController);
		 examGradesController.setPanel(examGradesPanel);
		 	 
		 studentPanelController.addObserver(examGradesPanel);
		 studentPanelController.addObserver(examGradesController);
		 studentsGroup.addObserver(examGradesPanel);
		 studentsGroup.addObserver(examGradesController);
		 
		 
		 ShowListsController showListsController = new ShowListsController();
		 
		 ShowListsPanel showFirstListsPanel = new ShowListsPanel(showListsController);
		 showListsController.setPanel(showFirstListsPanel);
		 
		 
		 studentPanelController.addObserver(showListsController);
		 studentsGroup.addObserver(showListsController);
		 profilePanelController.addObserver(showListsController);
		 
		 ConfirmationController confirmationController = new ConfirmationController();
		 ConfirmationPanel confirmationPanel = new ConfirmationPanel(confirmationController);
		 confirmationController.setPanel(confirmationPanel);
		 
		 studentPanelController.addObserver(confirmationPanel);
		 studentsGroup.addObserver(confirmationPanel);
		 
		 ShowFinalListsController showFinalListsController = new ShowFinalListsController();
		 ShowFinalListsPanel showFinalListsPanel = 
				 new ShowFinalListsPanel(showFinalListsController);
		 showFinalListsController.setPanel(showFinalListsPanel);
		 
		 studentPanelController.addObserver(showFinalListsController);
		 studentsGroup.addObserver(showFinalListsController);
		 profilePanelController.addObserver(showFinalListsController);
	
		MainPanel mainPanel = new MainPanel(studentPanel,profilePanel, examGradesPanel, showFirstListsPanel, 
				                           confirmationPanel, showFinalListsPanel, repository);
		
	}
	
	private void init() {
		
		File f = new File("./repository.ser");
        if(f.exists() && !f.isDirectory()) {
        	try {
        		FileInputStream fileIn = new FileInputStream("./repository.ser");
        		ObjectInputStream in = new ObjectInputStream(fileIn);
        		repository = (Repository) in.readObject();
        		in.close();
        		fileIn.close();

        	} catch (IOException | ClassNotFoundException i ) {

        		repository = new Repository();
        		i.printStackTrace();

        	}
        }else {
        	repository = new Repository();
        }
	}



}
