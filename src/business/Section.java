package business;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;

public class Section implements ISection, Serializable{
	
	private String name;
	private IPlaces places;
	private IEvaluation evaluation;

	private List<IStudent> studentsList; 
	private List<IStudent> finalList;  
	
	
	public Section(List<IStudent> studentsList2, List<IStudent> finalList2,
			IPlaces places2, IEvaluation evaluation2, String name2) {
		super();
		this.studentsList = studentsList2;
		this.finalList = finalList2;
		this.places = places2;
		this.evaluation = evaluation2;
		this.name = name2;
		updateNameSection();
	}

	public boolean needExam() {
		return evaluation.needExam();
	}
	
	public void updateNameSection() {
		
		name = name.concat(places.getType());
		
	}
	
	public void setIntoStudentsList(IStudent student) {

		studentsList.add(student);
		
	}
	
	public void setAddmGradeForStudents() {
		
		for (IStudent student : studentsList) {
			
			student.evaluationGrade(evaluation);
		}
	}

	@Override
	public boolean putStudentInHisFirstOption(IStudent student) {
		
		
		if (places.dontHavePlaces(finalList.size())) {
			
			return false;
		}
		
		finalList.add(student);
		
		return true;
		
		
	}
	
	public void refreshFinalList() {
		
		finalList.removeAll(finalList);
	}
	
	public boolean hasSameName(String nameProfile) {
		
		if(name.toLowerCase().contains(nameProfile.toLowerCase())) {
			return true;
		}
		
		return false;
	}
	
	
	public void printList() {
		
		for (IStudent student : finalList) {
			
			System.out.println(student.toString());
		}
	}
	
	public Vector<IStudent> getFinalVector() {
		Vector <IStudent> finalVector = new Vector(finalList);

		return finalVector;
		
	}

	@Override
	public String toString() {
		
		return name;
	}
}
