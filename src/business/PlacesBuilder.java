package business;

public class PlacesBuilder {
	
	public IPlaces createBudgetPlaces(int number) {
		
		return new BudgetPlaces(number);
	}
	
	public IPlaces createTaxPlaces ( int number ) {
		
		return new TaxPlaces(number);
	}

}
