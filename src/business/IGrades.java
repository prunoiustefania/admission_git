package business;

public interface IGrades {
	

	public void add (double grade);
	
	public void addExamGrade(double examGrade);
	
	public double getAvarageGrade();
	
	public double getExamGrade();

	public double getMathGrade();

}
