package business;

import java.util.List;

public class StudentBuilder {
	
	public IStudent create(String name, String cnp, IGrades grades, 
			List<ISection> sectionsList) {
		
		return new Student(name, cnp, grades, sectionsList);
	}

}
