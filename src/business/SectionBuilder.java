package business;

import java.util.ArrayList;

public class SectionBuilder {
	
	public ISection create(String sectionName, IPlaces places, IEvaluation evaluation) {
		
		return new Section(new ArrayList(), new ArrayList(), places, evaluation, sectionName);
		
	}

}
