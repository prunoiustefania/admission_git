package business;
import java.io.Serializable;
import java.util.List;

public class Student implements IStudent, Serializable{
	
	private String name, cnp;
	private IGrades grades;
	private List<ISection> sectionsChosenAsOptionsList;
	
	private boolean confirmationState = false;
	private double finalGrade;


	public Student(String name, String cnp, IGrades grades,
			List<ISection> sectionsChosenAsOptionsList) {
		super();
		this.name = name;
		this.cnp = cnp;
		this.grades = grades;
		this.sectionsChosenAsOptionsList = sectionsChosenAsOptionsList;
	}
	
	
	public void putInSectionsList() {
		for (ISection section : sectionsChosenAsOptionsList) {
			section.setIntoStudentsList(this);
		}
		
	}

	public boolean needExamGrade() {
		
		boolean suppose = false; 
		
		for (ISection section : sectionsChosenAsOptionsList) {
			if (suppose != section.needExam()) {
				return true;
			}
		}
		return suppose;
	}
	
	public void addExamGrade(Double grade) {
		// TODO Auto-generated method stub
		grades.addExamGrade(grade);
	}

	
	public void evaluationGrade(IEvaluation evaluation) { 
		
		finalGrade = evaluation.evaluationGrade(grades);
		
	}
	
	public int hasBetterMathGradeThan(IStudent student) {
		
		return student.isBetterMath(grades.getMathGrade());
	}
	

	@Override
	public int isBetterMath(double mathGrade) {
		// TODO Auto-generated method stub
		return (int) (grades.getMathGrade() - mathGrade);
	}
	

	public int hasBetterGradeThan( IStudent student) {
		return student.isBetter(finalGrade);
	}
	
	public int isBetter(double grade) {
		
		return (int) (finalGrade - grade);	
		
	}

	@Override
	public boolean putStudentInHisFirstOption() {
		
		if(finalGrade > 5) {                  
			for (ISection section : sectionsChosenAsOptionsList) {
			
				if(section.putStudentInHisFirstOption(this)) {
					
					return true;
					
				}
			}
			return false;
		}
		return false;
	}
	
	
	@Override
	public void confirmation(boolean a) {
		// TODO Auto-generated method stub
		confirmationState = a;
	}
	
	public boolean isConfirmed(boolean a) {
		
		return confirmationState == a;
	}
	
	@Override
	public String toString() {
		return "Student -> " + name + ", CNP = " + cnp;
	}
	
	public Double getExamGrade() {
		return grades.getExamGrade();
	}

	
}
