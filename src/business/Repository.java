package business;

import java.io.Serializable;
import java.util.ArrayList;

public class Repository implements Serializable{

	private StudentsGroup studentsGroup;
	private SectionsGroup sectionsGroup;
	
	
	public Repository() {
		super();
		studentsGroup = new StudentsGroup(new ArrayList<IStudent>(),
				new ArrayList<IStudent>(),new ArrayList<IStudent>());
		sectionsGroup = new SectionsGroup(new ArrayList<>());
	}
	
	public StudentsGroup getStudentsGroup() {
		return studentsGroup;
	}
	public void setStudentsGroup(StudentsGroup studentsGroup) {
		this.studentsGroup = studentsGroup;
	}
	public SectionsGroup getSectionsGroup() {
		return sectionsGroup;
	}
	public void setSectionsGroup(SectionsGroup sectionsGroup) {
		this.sectionsGroup = sectionsGroup;
	}
	
	
}
