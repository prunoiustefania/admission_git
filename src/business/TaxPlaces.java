package business;

import java.io.Serializable;

public class TaxPlaces implements IPlaces, Serializable{

	private int places;
	
	
	public TaxPlaces(int places) {
		super();
		this.places = places;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return " - Tax";
	}

	@Override
	public boolean dontHavePlaces(int size) {
		// TODO Auto-generated method stub
		return places <= size;
	}


}
