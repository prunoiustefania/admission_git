package business;
import java.util.List;
import java.util.Vector;

public interface ISection {
	
	public boolean needExam();

	public boolean putStudentInHisFirstOption(IStudent student);
	
	public void updateNameSection();
	
	public void setIntoStudentsList(IStudent student);
	
	public void setAddmGradeForStudents();
	
	public void refreshFinalList();
	
	public void printList();

	public Vector<IStudent> getFinalVector();
	
	public boolean hasSameName(String nameProfile);
}
