package business;
import java.util.List;

public class Manage {

	private StudentsGroup studentsGroup;
	private SectionsGroup sectionsGroup;
	
	
	public Manage(StudentsGroup studentsGroup, SectionsGroup sectionsGroup) {
		super();
		this.studentsGroup = studentsGroup;
		this.sectionsGroup = sectionsGroup;
	}

	// in main doar apelata
	
	public void setAdmGradeForStudents() {
		
		sectionsGroup.setAdmGradeForSections();
	}
	
	public void sortAndPrintStudents() {
		studentsGroup.sortList();
	}

	public void putStudentInHisFirstOption() {
		
		studentsGroup.putStudentInHisFirstOption();
	}
}
