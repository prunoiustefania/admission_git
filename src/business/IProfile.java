package business;

public interface IProfile {
	
	public boolean needExam();
	
	public void setIntoStudentsList(IStudent student);
	
}
