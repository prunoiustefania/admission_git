package business;

import java.io.Serializable;

public class BudgetPlaces implements IPlaces, Serializable {

	
	private int places;
	
	public BudgetPlaces(int places) {
		super();
		this.places = places;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return " - Budget";
	}

	@Override
	public boolean dontHavePlaces(int size) {
		// TODO Auto-generated method stub
		return places <= size;
	}
	
	

}
