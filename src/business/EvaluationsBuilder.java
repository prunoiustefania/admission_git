package business;

import java.util.Vector;

public class EvaluationsBuilder {
	
	private Vector<IEvaluation> evalVector;
	
	private AvarageEvaluation avarageEval;
	private ExamAvarageEvaluation examAvarageEval;
	private ExamEvaluation examEval;
	
	public void build() {
		
		Vector<IEvaluation> evalVector = new Vector<IEvaluation>();
		
		AvarageEvaluation avarageEval = new AvarageEvaluation();
		ExamAvarageEvaluation examAvarageEval = new ExamAvarageEvaluation();
		ExamEvaluation examEval = new ExamEvaluation();
	}
	
	public Vector<IEvaluation> createVector(){
		
		build();
		Vector<IEvaluation> evalVector = new Vector<IEvaluation>();
		
		AvarageEvaluation avarageEval = new AvarageEvaluation();
		ExamAvarageEvaluation examAvarageEval = new ExamAvarageEvaluation();
		ExamEvaluation examEval = new ExamEvaluation();
		evalVector.add(avarageEval);
		evalVector.add(examAvarageEval);
		evalVector.add(examEval);
		
		return evalVector;
		
	}

}
