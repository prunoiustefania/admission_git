package business;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.imageio.stream.FileCacheImageInputStream;


public class StudentsGroup extends Observable implements Serializable {
	
	private List<IStudent> studentsGroup;
	private List<IStudent> studentsWhoNeedExamGroup;
	private List<IStudent> potentialAdmiss;
	


	public StudentsGroup(List<IStudent> studentsGroup, List<IStudent> studentsWhoNeedExamGroup,
			List<IStudent> potentialAdmiss) {
		super();
		this.studentsGroup = studentsGroup;
		this.studentsWhoNeedExamGroup = studentsWhoNeedExamGroup;
		this.potentialAdmiss = potentialAdmiss;
	}

	public void addToGroup(IStudent student) {
		studentsGroup.add(student);
	}
	
	public void addToExamGroup(IStudent student) {
		
		if (student.needExamGrade()) {
			studentsWhoNeedExamGroup.add(student);
			
		}
	 }
	
	// for examGradesPanel
	public Vector<IStudent> getStudentsWhoNeedExamGroup(){
		
		Vector studentsVectorWhoNeedExam = new Vector(studentsWhoNeedExamGroup);
		return studentsVectorWhoNeedExam;
	}
	
	//for confirmationPanel
	
	public Vector<IStudent> getAllStudents(){
		
		Vector allStudents = new Vector(studentsGroup);
		return allStudents;
	}
	
	public Vector<IStudent> getPotentialAdd(){
		Vector potentialAdmis = new Vector(potentialAdmiss);
		return potentialAdmis;
	}
	
	public void sortList() {
		Collections.sort(studentsGroup, new Comparator<IStudent>(){
			public int compare(IStudent s1, IStudent s2) {
				
				int auxVariable = s1.hasBetterGradeThan(s2);
				if(auxVariable != 0 ) {
					return auxVariable;
				}
				
				return s1.hasBetterMathGradeThan(s2);
			}
		});
	}
	


	public void putStudentInHisFirstOption() {
		
		for (IStudent student : studentsGroup) {
			
			if(student.putStudentInHisFirstOption() == false) {
				potentialAdmiss.add(student);
			}
		}
		
	}
	
	public void confirmation() {
		
		for(int index = 0; index < studentsGroup.size(); index++) {
			
			IStudent student = studentsGroup.get(index);
			
			if(student.isConfirmed(false)) {
				
				studentsGroup.remove(student);
			}
		}
	}
	

/*	public void printList() {
		
		for (IStudent student : studentsGroup) {
			
			System.out.println(student.toString());
		}
		
		System.out.println(" ");
		
	}
	
	public void printExamList() {
		for (IStudent student : studentsWhoNeedExamGroup) {
			
			System.out.println(student.toString());
		}
		System.out.println(" ");
	}
	
	public void printPotentialAdmmList() {
		
		for (IStudent student : potentialAdmiss) {
			
			System.out.println(student.toString());
		}
		
		System.out.println(" ");
		
		
	}*/
	
}
