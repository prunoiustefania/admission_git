package business;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Grades implements IGrades, Serializable {
	
	private List<Double> gradesList;
	
	
	public Grades(List<Double> gradesList) {
		super();
		this.gradesList = gradesList;
	}
	
	public void add (double grade) {
		gradesList.add(grade);
	}
	
	public void addExamGrade(double examGrade) {
		gradesList.add(examGrade);         
	}
	
	public double getAvarageGrade() {
		
		return gradesList.get(0);
	}
	
	public double getExamGrade() {
		
		return gradesList.get(gradesList.size() -1 );
	}

	@Override
	public double getMathGrade() {
		// TODO Auto-generated method stub
		return gradesList.get(1);
	}

}
